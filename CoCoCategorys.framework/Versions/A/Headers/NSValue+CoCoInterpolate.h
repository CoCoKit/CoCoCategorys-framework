//
//  NSValue+CoCoInterpolate.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/7/7.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoCoCategorys/CoCoInterpolatable.h>

@interface NSValue (CoCoInterpolate)<CoCoInterpolatable>
- (NSValue *)interpolateTo:(NSValue *)toValue withProgress:(CGFloat)progress;

+ (CGPoint)interpolateCGPointFrom:(CGPoint)fromValue to:(CGPoint)toValue withProgress:(CGFloat)progress;

+ (CGSize)interpolateCGSizeFrom:(CGSize)fromValue to:(CGSize)toValue withProgress:(CGFloat)progress;

+ (CGRect)interpolateCGRectFrom:(CGRect)fromValue to:(CGRect)toValue withProgress:(CGFloat)progress;

+ (CGVector)interpolateCGVectorFrom:(CGVector)fromValue to:(CGVector)toValue withProgress:(CGFloat)progress;

+ (UIEdgeInsets)interpolateUIEdgeInsetsFrom:(UIEdgeInsets)fromValue to:(UIEdgeInsets)toValue withProgress:(CGFloat)progress;

+ (UIOffset)interpolateUIOffsetFrom:(UIOffset)fromValue to:(UIOffset)toValue withProgress:(CGFloat)progress;

+ (CGAffineTransform)interpolateCGAffineTransformFrom:(CGAffineTransform)fromValue to:(CGAffineTransform)toValue withProgress:(CGFloat)progress;
@end
