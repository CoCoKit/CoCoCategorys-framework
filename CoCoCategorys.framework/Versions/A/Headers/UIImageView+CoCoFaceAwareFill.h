//
//  UIImageView+CoCoFaceAwareFill.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (CoCoFaceAwareFill)
// 第一次使用前调用一下，避免+load 和 +initialize
+ (void)firstLoad;

- (void)coco_faceAwareFill;
@end
