//
//  NSString+CoCoPath.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/2/22.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoPath)
/**
 * 检查目录是否存在，不存在则创建文件夹
 */
- (BOOL)coco_checkDirPath;

- (BOOL)coco_removeAllFile;
@end
