//
//  CoCoMacroConfig.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CGBase.h>

CGFloat KSCREEN_CHOOSE_FLOOT(CGFloat inch35, CGFloat inch40, CGFloat inch47, CGFloat inch55, CGFloat iPhoneX);
int KSCREEN_CHOOSE_INT(int inch35, int inch40, int inch47, int inch55, int iPhoneX);


@interface CoCoMacroConfig : NSObject

@end
