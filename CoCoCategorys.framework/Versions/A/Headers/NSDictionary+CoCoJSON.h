//
//  NSDictionary+CoCoJSON.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (CoCoJSON)
/**
 *  @brief NSDictionary转换成JSON字符串
 *
 *  @return  JSON字符串
 */
- (NSString *)coco_toJSONString;
@end
