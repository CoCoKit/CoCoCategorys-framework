//
//  UIDevice+Version.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/9/24.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM (NSInteger, CoCoDeviceVersion){
    CoCoDeviceVersionUnknownDevice         = 0,
    CoCoDeviceVersionSimulator             = 1,

    CoCoDeviceVersioniPhone4               = 3,
    CoCoDeviceVersioniPhone4S              = 4,
    CoCoDeviceVersioniPhone5               = 5,
    CoCoDeviceVersioniPhone5C              = 6,
    CoCoDeviceVersioniPhone5S              = 7,
    CoCoDeviceVersioniPhone6               = 8,
    CoCoDeviceVersioniPhone6Plus           = 9,
    CoCoDeviceVersioniPhone6S              = 10,
    CoCoDeviceVersioniPhone6SPlus          = 11,
    CoCoDeviceVersioniPhone7               = 12,
    CoCoDeviceVersioniPhone7Plus           = 13,
    CoCoDeviceVersioniPhone8               = 14,
    CoCoDeviceVersioniPhone8Plus           = 15,
    CoCoDeviceVersioniPhoneX               = 16,
    CoCoDeviceVersioniPhoneSE              = 17,

    CoCoDeviceVersioniPad1                 = 18,
    CoCoDeviceVersioniPad2                 = 19,
    CoCoDeviceVersioniPadMini              = 20,
    CoCoDeviceVersioniPad3                 = 21,
    CoCoDeviceVersioniPad4                 = 22,
    CoCoDeviceVersioniPadAir               = 23,
    CoCoDeviceVersioniPadMini2             = 24,
    CoCoDeviceVersioniPadAir2              = 25,
    CoCoDeviceVersioniPadMini3             = 26,
    CoCoDeviceVersioniPadMini4             = 27,
    CoCoDeviceVersioniPadPro12Dot9Inch     = 28,
    CoCoDeviceVersioniPadPro9Dot7Inch      = 29,
    CoCoDeviceVersioniPad5                 = 30,
    CoCoDeviceVersioniPadPro12Dot9Inch2Gen = 31,
    CoCoDeviceVersioniPadPro10Dot5Inch     = 32,

    CoCoDeviceVersioniPodTouch1Gen         = 33,
    CoCoDeviceVersioniPodTouch2Gen         = 34,
    CoCoDeviceVersioniPodTouch3Gen         = 35,
    CoCoDeviceVersioniPodTouch4Gen         = 36,
    CoCoDeviceVersioniPodTouch5Gen         = 37,
    CoCoDeviceVersioniPodTouch6Gen         = 38
};

typedef NS_ENUM (NSInteger, CoCoDeviceSize){
    CoCoDeviceSizeUnknownSize     = 0,
    CoCoDeviceSizeScreen3Dot5inch = 1,
    CoCoDeviceSizeScreen4inch     = 2,
    CoCoDeviceSizeScreen4Dot7inch = 3,
    CoCoDeviceSizeScreen5Dot5inch = 4,
    CoCoDeviceSizeScreen5Dot8inch = 5
};

@interface UIDevice (Version)
+ (CoCoDeviceVersion)deviceVersion;
+ (NSString *)deviceNameForVersion:(CoCoDeviceVersion)CoCoDeviceVersion;
+ (CoCoDeviceSize)resolutionSize;
+ (CoCoDeviceSize)deviceSize;
+ (NSString *)deviceSizeName:(CoCoDeviceSize)deviceSize;
+ (NSString *)deviceNameString;
+ (BOOL)isZoomed;

+ (BOOL)versionEqualTo:(NSString *)version;
+ (BOOL)versionGreaterThan:(NSString *)version;
+ (BOOL)versionGreaterThanOrEqualTo:(NSString *)version;
+ (BOOL)versionLessThan:(NSString *)version;
+ (BOOL)versionLessThanOrEqualTo:(NSString *)version;
@end
