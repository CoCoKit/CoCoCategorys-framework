//
//  NSObject+CoCoAccociated.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/6/26.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (CoCoAccociated)
- (id)coco_object;

- (void)coco_setObject:(id)object;

- (id)coco_block;

- (void)coco_setBlock:(id)block;


/**
 *  @brief  附加一个stong对象
 *
 *  @param value 被附加的对象
 *  @param key   被附加对象的key
 */
- (void)coco_associateValue:(id)value withKey:(void *)key; // Strong reference
/**
 *  @brief  附加一个weak对象
 *
 *  @param value 被附加的对象
 *  @param key   被附加对象的key
 */
- (void)coco_weaklyAssociateValue:(id)value withKey:(void *)key;

/**
 *  @brief  根据附加对象的key取出附加对象
 *
 *  @param key 附加对象的key
 *
 *  @return 附加对象
 */
- (id)coco_associatedValueForKey:(void *)key;
@end
