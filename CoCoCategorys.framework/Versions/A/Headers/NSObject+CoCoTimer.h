//
//  NSObject+CoCoTimer.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/6/26.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CoCoTimerActionBlock)(NSUInteger times);
typedef void (^CoCoTimerCompleteBlock)(void);

@interface NSObject (CoCoTimer)

- (void)stopTimer;
- (void)startTimerDuration:(NSTimeInterval)duration times:(NSInteger)times
    action:(CoCoTimerActionBlock)durationBlock
    complete:(CoCoTimerCompleteBlock)completeBlock;

@end
