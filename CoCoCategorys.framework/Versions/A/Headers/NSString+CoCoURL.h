//
//  NSString+CoCoURL.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoURL)
- (BOOL)coco_isValidURL;

- (NSString *)coco_urlEncode;
- (NSString *)coco_urlEncodeUsingEncoding:(NSStringEncoding)encoding;

- (NSString *)coco_urlDecode;
- (NSString *)coco_urlDecodeUsingEncoding:(NSStringEncoding)encoding;

- (NSDictionary *)coco_dictionaryFromURLParameters;
@end
