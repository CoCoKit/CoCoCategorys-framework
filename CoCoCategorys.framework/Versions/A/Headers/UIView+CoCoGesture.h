//
//  UIView+CoCoGesture.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^CoCoGestureActionBlock)(UIGestureRecognizer *gestureRecoginzer);

@interface UIView (CoCoGesture)
/**
 *  @brief  添加tap手势
 *
 *  @param block 代码块
 */
- (void)coco_addTapActionWithBlock:(CoCoGestureActionBlock)block;
/**
 *  @brief  添加长按手势
 *
 *  @param block 代码块
 */
- (void)coco_addLongPressActionWithBlock:(CoCoGestureActionBlock)block;
@end
