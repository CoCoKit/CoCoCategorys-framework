//
//  UIImageView+CoCoBetterFace.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (CoCoBetterFace)
@property (nonatomic) BOOL coco_needsBetterFace;
@property (nonatomic) BOOL coco_fast;
- (void)coco_setBetterFaceImage:(UIImage *)image;
@end
