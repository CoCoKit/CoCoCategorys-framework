//
//  UIView+CoCoSnapshot.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CoCoSnapshot)
- (UIImage *)coco_snapshot;
- (UIImage *)coco_snapshotImageAfterScreenUpdates:(BOOL)afterUpdates;
@end
