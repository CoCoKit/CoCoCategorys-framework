//
//  NSString+CoCoNumber.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoNumber)
- (BOOL)coco_isNumber;
- (BOOL)coco_isPureFloat;
- (BOOL)coco_isPureInt;
@end
