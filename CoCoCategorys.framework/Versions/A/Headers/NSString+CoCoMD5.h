//
//  NSString+CoCoMD5.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/2.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoMD5)
@property (nonatomic, copy, readonly) NSString *coco_md5;
@end
