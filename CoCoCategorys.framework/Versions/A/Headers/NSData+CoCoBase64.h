//
//  NSData+CoCoBase64.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/25.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (CoCoBase64)

- (nullable NSString *)coco_base64EncodedString;

+ (nullable NSData *)coco_dataWithBase64EncodedString:(NSString *)base64EncodedString;
@end
