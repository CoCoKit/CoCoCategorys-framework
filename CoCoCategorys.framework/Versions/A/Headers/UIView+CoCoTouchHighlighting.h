//
//  UIView+CoCoTouchHighlighting.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM (NSInteger, CoCoHighlightingStyle) {
    CoCoHighlightingStyleNone              = 0,
    CoCoHighlightingStyleTransparentMask   = 1,
    CoCoHighlightingStyleLightBackground   = 2,
    CoCoHighlightingStyleSolidDarkOverlay  = 3,
    CoCoHighlightingStyleHollowDarkOverlay = 4,
};

@interface UIView (CoCoTouchHighlighting)

@property (nonatomic, assign) CoCoHighlightingStyle touchHighlightingStyle;

@end
