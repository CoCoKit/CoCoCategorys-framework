//
//  NSMutableArray+CoCoSafe.h
//  WeiboCoCo
//
//  Created by 陈明 on 2017/5/29.
//  Copyright © 2017年 com.weibococo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (CoCoSafe)
/*!
 *  向数组中添加对象；如果该对象为nil，则不添加。
 *
 *  @param obj 要添加的对象
 */
- (void)coco_addSafeObject:(id)obj;
@end
