//
//  UIView+CoCoNib.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CoCoNib)
+ (UINib *)coco_loadNib;
+ (UINib *)coco_loadNibNamed:(NSString *)nibName;
+ (UINib *)coco_loadNibNamed:(NSString *)nibName bundle:(NSBundle *)bundle;

+ (instancetype)coco_loadInstanceFromNib;
+ (instancetype)coco_loadInstanceFromNibWithName:(NSString *)nibName;
+ (instancetype)coco_loadInstanceFromNibWithName:(NSString *)nibName owner:(id)owner;
+ (instancetype)coco_loadInstanceFromNibWithName:(NSString *)nibName owner:(id)owner bundle:(NSBundle *)bundle;
@end
