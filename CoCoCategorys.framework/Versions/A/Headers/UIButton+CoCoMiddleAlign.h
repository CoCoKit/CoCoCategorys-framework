//
//  UIButton+CoCoMiddleAlign.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (CoCoMiddleAlign)
/**
   @param spacing The middle spacing between imageView and titleLabel.
   @discussion The middle aligning method for imageView and titleLabel.
 */
- (void)coco_middleAlignButtonWithSpacing:(CGFloat)spacing;
@end
