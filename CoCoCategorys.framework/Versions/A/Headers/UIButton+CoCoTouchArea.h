//
//  UIButton+CoCoTouchAreInsets.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (CoCoTouchAreInsets)
/**
 *  设置按钮额外热区
 */

- (void)coco_setTouchAreaEdge:(CGFloat)size;
- (void)coco_setTouchAreaEdgeWithTop:(CGFloat)top right:(CGFloat)right bottom:(CGFloat)bottom left:(CGFloat)left;
- (UIEdgeInsets)coco_touchAreaEdgeInsets;
- (CGRect)coco_touchAreaRect;
@end
