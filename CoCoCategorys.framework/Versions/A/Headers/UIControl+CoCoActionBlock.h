//
//  UIControl+CoCoActionBlock.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^UIControlCoCoActionBlock)(id weakSender);


@interface UIControlCoCoActionBlockWrapper : NSObject
@property (nonatomic, copy) UIControlCoCoActionBlock coco_actionBlock;
@property (nonatomic, assign) UIControlEvents coco_controlEvents;
- (void)coco_invokeBlock:(id)sender;
@end

@interface UIControl (CoCoActionBlock)
- (void)coco_touchDown:(void (^)(void))eventBlock;
- (void)coco_touchDownRepeat:(void (^)(void))eventBlock;
- (void)coco_touchDragInside:(void (^)(void))eventBlock;
- (void)coco_touchDragOutside:(void (^)(void))eventBlock;
- (void)coco_touchDragEnter:(void (^)(void))eventBlock;
- (void)coco_touchDragExit:(void (^)(void))eventBlock;
- (void)coco_touchUpInside:(void (^)(void))eventBlock;
- (void)coco_touchUpOutside:(void (^)(void))eventBlock;
- (void)coco_touchCancel:(void (^)(void))eventBlock;
- (void)coco_valueChanged:(void (^)(void))eventBlock;
- (void)coco_editingDidBegin:(void (^)(void))eventBlock;
- (void)coco_editingChanged:(void (^)(void))eventBlock;
- (void)coco_editingDidEnd:(void (^)(void))eventBlock;
- (void)coco_editingDidEndOnExit:(void (^)(void))eventBlock;


- (void)coco_handleControlEvents:(UIControlEvents)controlEvents withBlock:(UIControlCoCoActionBlock)actionBlock;
- (void)coco_removeActionBlocksForControlEvents:(UIControlEvents)controlEvents;
@end
