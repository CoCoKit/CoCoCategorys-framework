//
//  UIApplication+CoCoPermissions.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
    CoCoPermissionTypeBluetoothLE,
    CoCoPermissionTypeCalendar,
    CoCoPermissionTypeContacts,
    CoCoPermissionTypeLocation,
    CoCoPermissionTypeMicrophone,
    CoCoPermissionTypeMotion,
    CoCoPermissionTypePhotos,
    CoCoPermissionTypeReminders,
} CoCoPermissionType;

typedef enum {
    CoCoPermissionAccessDenied, // User has rejected feature
    CoCoPermissionAccessGranted, // User has accepted feature
    CoCoPermissionAccessRestricted, // Blocked by parental controls or system settings
    CoCoPermissionAccessUnknown, // Cannot be determined
    CoCoPermissionAccessUnsupported, // Device doesn't support this - e.g Core Bluetooth
    CoCoPermissionAccessMissingFramework, // Developer didn't import the required framework to the project
} CoCoPermissionAccess;

@interface UIApplication (CoCoPermissions)
// Check permission of service. Cannot check microphone or motion without asking user for permission
- (CoCoPermissionAccess)coco_hasAccessToBluetoothLE;
- (CoCoPermissionAccess)coco_hasAccessToCalendar;
- (CoCoPermissionAccess)coco_hasAccessToContacts;
- (CoCoPermissionAccess)coco_hasAccessToLocation;
- (CoCoPermissionAccess)coco_hasAccessToPhotos;
- (CoCoPermissionAccess)coco_hasAccessToReminders;
- (CoCoPermissionAccess)coco_hasAccessToCamera;

// Request permission with callback
- (void)coco_requestAccessToCalendarWithSuccess:(void (^)(void))accessGranted andFailure:(void (^)(void))accessDenied;
- (void)coco_requestAccessToContactsWithSuccess:(void (^)(void))accessGranted andFailure:(void (^)(void))accessDenied;
- (void)coco_requestAccessToMicrophoneWithSuccess:(void (^)(void))accessGranted andFailure:(void (^)(void))accessDenied;
- (void)coco_requestAccessToPhotosWithSuccess:(void (^)(void))accessGranted andFailure:(void (^)(void))accessDenied;
- (void)coco_requestAccessToRemindersWithSuccess:(void (^)(void))accessGranted andFailure:(void (^)(void))accessDenied;
- (void)coco_requestAccessToCameraWithSuccess:(void (^)(void))accessGranted andFailure:(void (^)(void))accessDenied;

// Instance methods
- (void)coco_requestAccessToLocationWithSuccess:(void (^)(void))accessGranted andFailure:(void (^)(void))accessDenied;

// No failure callback available
- (void)coco_requestAccessToMotionWithSuccess:(void (^)(void))accessGranted;

// Needs investigating - unsure whether it can be implemented because of required delegate callbacks
// -(void)requestAccessToBluetoothLEWithSuccess:(void(^)(void))accessGranted;
@end
