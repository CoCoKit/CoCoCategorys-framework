//
//  UITextField+CoCoSelect.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (CoCoSelect)
/**
 *  @brief  当前选中的字符串范围
 *
 *  @return NSRange
 */
- (NSRange)coco_selectedRange;
/**
 *  @brief  选中所有文字
 */
- (void)coco_selectAllText;
/**
 *  @brief  选中指定范围的文字
 *
 *  @param range NSRange范围
 */
- (void)coco_setSelectedRange:(NSRange)range;
@end
