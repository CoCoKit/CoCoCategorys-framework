//
//  UIImage+CoCoSize.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CoCoSize)

- (nullable UIImage *)coco_resizeImageWithEdge:(UIEdgeInsets)insets withColor:(UIColor *)color;
- (nullable UIImage *)coco_resizeImageWithEdge:(UIEdgeInsets)insets scale:(CGFloat)scale withColor:(UIColor *)color;

/**
 * 修改图片尺寸， 缩放系数用当前比例系数
 */
- (nullable UIImage *)coco_resizeToSize:(CGSize)size;
- (nullable UIImage *)coco_resizeToSize:(CGSize)size scale:(CGFloat)scale;
- (nullable UIImage *)coco_resizeToSize:(CGSize)size contentMode:(UIViewContentMode)contentMode;
- (nullable UIImage *)coco_resizeToSize:(CGSize)size scale:(CGFloat)scale contentMode:(UIViewContentMode)contentMode;

- (nullable UIImage *)coco_cropImageToRect:(CGRect)rect;
- (nullable UIImage *)coco_cropImageToRect:(CGRect)rect scale:(CGFloat)scale;
/**
 *  fitSize     YES: new image's size is extend to fit all content.
                NO: image's size will not change, content may be clipped.
 */
- (nullable UIImage *)coco_imageByRotate:(CGFloat)radians fitSize:(BOOL)fitSize;
- (nullable UIImage *)coco_imageByRotateLeft90;
- (nullable UIImage *)coco_imageByRotateRight90;
- (nullable UIImage *)coco_imageByRotate180;
- (nullable UIImage *)coco_imageByFlipVertical;
- (nullable UIImage *)coco_imageByFlipHorizontal;
@end
