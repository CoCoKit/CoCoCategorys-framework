//
//  NSObject+CoCoSafeBlock.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/2.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CoCoSafeBlockDeallocBlock)(NSString *identifier);

extern NSString *const CoCoSafeBlockObjectDidDeallocNotification;

@interface NSObject (CoCoSafeBlock)
- (NSString *)coco_safeBlockIdentifier;
- (NSString *)coco_safeBlockIdentifierCreate:(BOOL)create;
- (void)coco_setSafeBlockDeallocBlock:(CoCoSafeBlockDeallocBlock)deallocBlock;
@end


id get_safe_block_object(NSString *coco_safeBlockIdentifier);
BOOL is_safe_block_object_still_alive(NSString *coco_safeBlockIdentifier);
void dispatch_async_safe(NSString *coco_safeBlockIdentifier, dispatch_queue_t queue, dispatch_block_t block);
void dispatch_after_safe(NSString *coco_safeBlockIdentifier, dispatch_time_t when, dispatch_queue_t queue, dispatch_block_t block);
