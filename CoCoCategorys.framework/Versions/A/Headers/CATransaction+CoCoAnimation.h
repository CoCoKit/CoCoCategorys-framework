//
//  CATransaction+CoCoAnimation.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CATransaction (CoCoAnimation)
+ (void)coco_animateWithDuration:(NSTimeInterval)duration
    animations:(void (^)(void))animations
    completion:(void (^)(void))completion;
@end
