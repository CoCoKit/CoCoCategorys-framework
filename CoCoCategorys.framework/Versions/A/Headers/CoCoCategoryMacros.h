//
//  CoCoMacros.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#ifndef CoCoMacros_h
#define CoCoMacros_h


#import <CoCoCategorys/CoCoMacroConfig.h>

#endif /* CoCoMacros_h */




// #ifdef DEBUG
// #define NSLog(FORMAT, ...) fprintf(stderr,"%s\n-----------------------------------------------\n",[[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
// #else
// #define NSLog(...)
// #endif

// 设备
// 判断是否为iPhone
#define IS_IPHONE              (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
// 判断是否为iPad
#define IS_IPAD                (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
// 判断是否为iPod
#define IS_IPOD                ([[[UIDevice currentDevice] model] isEqualToString:@"iPod touch"])
// 判断是否为 iPhone 4
#define IS_IPHONE_4_OR_LESS    (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
// 判断是否为 iPhone 5SE
#define iPhone5SE              [[UIScreen mainScreen] bounds].size.width == 320.0f && [[UIScreen mainScreen] bounds].size.height == 568.0f
// 判断是否为iPhone 6/6s
#define iPhone6_6s             [[UIScreen mainScreen] bounds].size.width == 375.0f && [[UIScreen mainScreen] bounds].size.height == 667.0f
// 判断是否为iPhone 6Plus/6sPlus
#define iPhone6Plus_6sPlus     [[UIScreen mainScreen] bounds].size.width == 414.0f && [[UIScreen mainScreen] bounds].size.height == 736.0f
// 判断 iOS 8 或更高的系统版本
#define IOS_VERSION_8_OR_LATER (([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) ? (YES) : (NO))


// 屏幕参数选择
#define SCREEN_CHOOSE_FLOOT(a, b, c, d, e) KSCREEN_CHOOSE_FLOOT(a, b, c, d, e)
#define SCREEN_CHOOSE_INT(a, b, c, d, e)   KSCREEN_CHOOSE_INT(a, b, c, d, e)

// 屏幕
#define IS_RETINA              ([[UIScreen mainScreen] scale] >= 2.0)
#define KPixel                 (1 / [UIScreen mainScreen].scale)
#define KScale                 [UIScreen mainScreen].scale
#define SCREEN_WIDTH           [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT          [[UIScreen mainScreen] bounds].size.height
#define SCREEN_MAX_LENGTH      (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH      (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))


/**
 *  app version
 */
#define APP_SHORT_VERSION      [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
#define APP_BUNDLE_VERSION     [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]
#define APP_VERSION            [NSString stringWithFormat:@"%@ (%@)", APP_SHORT_VERSION, APP_BUNDLE_VERSION]
#define APP_BUNDLE_IDENTIFIER  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]
/**
 * 1. if (NSOrderedAscending != COMPARE_VERSION(v1,v2))  { //v1 >= v2 }
 * 2. if (NSOrderedDescending == COMPARE_VERSION(v1,v2)) { //v1 > v2 }
 * 3. if (NSOrderedAscending == COMPARE_VERSION(v1,v2))  { //v1 < v2 }
 */
#define COMPARE_VERSION(v1, v2)                    [v1 compare: v2 options: NSNumericSearch]
#define COMPARE_CURRENT_VERSION(v)                 COMPARE_VERSION(APP_SHORT_VERSION, v)

// 获取类名
#define ClassName(name)                            (NSStringFromClass([name class]))

// 检查系统版本
#define SYSTEM_VERSION_EQUAL_TO(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)             ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)    ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
