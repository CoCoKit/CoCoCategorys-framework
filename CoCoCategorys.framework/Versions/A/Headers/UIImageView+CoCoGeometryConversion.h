//
//  UIImageView+CoCoGeometryConversion.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (CoCoGeometryConversion)
- (CGPoint)coco_convertPointFromImage:(CGPoint)imagePoint;
- (CGRect)coco_convertRectFromImage:(CGRect)imageRect;

- (CGPoint)coco_convertPointFromView:(CGPoint)viewPoint;
- (CGRect)coco_convertRectFromView:(CGRect)viewRect;
@end
