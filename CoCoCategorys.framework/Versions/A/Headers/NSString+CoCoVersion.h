//
//  NSString+CoCoVersion.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/2.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoVersion)

- (NSComparisonResult)coco_compareToVersion:(NSString *)version;

- (BOOL)coco_isOlderThanVersion:(NSString *)version;
- (BOOL)coco_isNewerThanVersion:(NSString *)version;
- (BOOL)coco_isEqualToVersion:(NSString *)version;
- (BOOL)coco_isEqualOrOlderThanVersion:(NSString *)version;
- (BOOL)coco_isEqualOrNewerThanVersion:(NSString *)version;

- (NSString *)coco_getMainVersionWithIntegerCount:(NSInteger)integerCount;
- (BOOL)coco_needsToUpdateToVersion:(NSString *)newVersion mainVersionIntegerCount:(NSInteger)integerCount;

@end
