//
//  NSObject+KVO.h
//  ImplementKVO
//
//  Created by Peng Gu on 2/26/15.
//  Copyright (c) 2015 Peng Gu. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CoCoObservingBlock)(id observedObject, NSString *observedKey, id oldValue, id newValue);

@interface NSObject (CoCoKVO)

- (void)coco_addObserver:(NSObject *)observer
    forKey:(NSString *)key
    withBlock:(CoCoObservingBlock)block;

- (void)coco_removeObserver:(NSObject *)observer forKey:(NSString *)key;

@end
