//
//  UIImage+CoCoColor.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CoCoColor)
+ (UIImage *)imageWithColor:(UIColor *)color;
// 给我一种颜色，一个尺寸，我给你返回一个UIImage:不透明
+ (UIImage *)imageFromContextWithColor:(UIColor *)color;
+ (UIImage *)imageFromContextWithColor:(UIColor *)color size:(CGSize)size;
+ (UIImage *)imageFromContextWithColor:(UIColor *)color size:(CGSize)size scale:(CGFloat)scale;
@end
