//
//  UIDevice+CoCoBase.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

#define KDevice_UDID          [UIDevice UDID]
#define KDevice_SystemName    [UIDevice currentDevice].systemName
#define KDevice_SystemVersion [UIDevice currentDevice].systemVersion


@interface UIDevice (CoCoBase)
+ (NSString *)UDID;
@end
