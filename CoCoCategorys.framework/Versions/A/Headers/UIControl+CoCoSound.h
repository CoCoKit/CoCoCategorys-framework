//
//  UIControl+CoCoSound.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIControl (CoCoSound)
//不同事件增加不同声音
- (void)coco_setSoundNamed:(NSString *)name forControlEvent:(UIControlEvents)controlEvent;
@end
