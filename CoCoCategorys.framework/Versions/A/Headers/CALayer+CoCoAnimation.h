//
//  CALayer+Animation.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/4/14.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
@interface CALayer (CoCoAnimation)

/**
   添加一个曲线动画
 */
- (void)coco_addFadeAnimationWithDuration:(NSTimeInterval)duration
    curve:(UIViewAnimationCurve)curve;


/**
   移除曲线动画
 */
- (void)coco_removePreviousFadeAnimation;

/**
 *  颤抖效果
 */
- (CAAnimation *)coco_shakeFunction;

/**
 *  渐显效果
 */
- (CATransition *)coco_fadeFunction;

/**
 *  渐显效果 效果时间
 */
- (CATransition *)coco_fadeFunction:(CGFloat)time;

/**
 *  缩放效果
 */
- (CAKeyframeAnimation *)coco_transformScaleFunction;




@property (nonatomic) CGFloat transformScale;// key path "tranform.scale"
@end
