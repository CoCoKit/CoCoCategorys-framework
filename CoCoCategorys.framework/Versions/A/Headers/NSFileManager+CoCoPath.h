//
//  NSFileManager+CoCoPath.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/4/26.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (CoCoPath)
/**
 * 检查并创建文件夹
 */
+ (BOOL)coco_createDirectoryIfNotExist:(NSString *)directory;

+ (unsigned long long)coco_sizeOfFolder:(NSString *)folderPath;
@end
