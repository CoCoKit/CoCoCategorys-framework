//
//  NSDictionary+CoCoEnumer.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (CoCoEnumer)
- (void)coco_each:(void (^)(id k, id v))block;
- (void)coco_eachKey:(void (^)(id k))block;
- (void)coco_eachValue:(void (^)(id v))block;
- (NSArray *)coco_map:(id (^)(id key, id value))block;
- (NSDictionary *)coco_pick:(NSArray *)keys;
- (NSDictionary *)coco_omit:(NSArray *)key;
@end
