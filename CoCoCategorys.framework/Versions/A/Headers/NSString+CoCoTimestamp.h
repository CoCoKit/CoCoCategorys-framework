//
//  NSString+CoCoTimestamp.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoTimestamp)
+ (NSString *)CoCo_StringForTimeSince1970;
/**
 *
 *  @brief  毫秒时间戳 例如 1443066826371
 *
 *  @return 毫秒时间戳
 */
+ (NSString *)CoCo_StringNowTimestamp;
// 毫秒时间戳
+ (NSString *)CoCo_StringDateTimestamp:(NSDate *)date;
@end
