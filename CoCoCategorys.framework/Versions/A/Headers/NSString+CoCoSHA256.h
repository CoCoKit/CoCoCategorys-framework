//
//  NSString+CoCoSHA256.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoSHA256)
- (NSString *)coco_sha256;
@end
