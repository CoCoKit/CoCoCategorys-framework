//
//  NSString+CoCoMobileNumber.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoMobileNumber)
// 手机号有效性
- (BOOL)coco_isMobileNumber;
// 手机号分服务商
- (BOOL)coco_isMobileNumberClassification;
@end
