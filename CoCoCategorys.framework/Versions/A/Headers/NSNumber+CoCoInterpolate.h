//
//  NSNumber+CoCoInterpolate.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/7/7.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGBase.h>
#import <CoCoCategorys/CoCoInterpolatable.h>

@interface NSNumber (CoCoInterpolate)<CoCoInterpolatable>
- (NSNumber *)interpolateTo:(NSNumber *)toValue withProgress:(CGFloat)progress;
+ (CGFloat)interpolateCGFloatFrom:(CGFloat)fromValue to:(CGFloat)toValue withProgress:(CGFloat)progress;
@end
