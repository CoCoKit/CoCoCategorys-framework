//
//  UIColor+CoCoBase.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>


#define HexColorWithAlpha(hexValue, a) [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16)) / 255.0 green: ((float)((hexValue & 0x00FF00) >> 8)) / 255.0 blue: ((float)(hexValue & 0x0000FF)) / 255.0 alpha: a]
#define HexColor(hexValue)             HexColorWithAlpha(hexValue, 1.0)
#define RGBAColor(r, g, b, a)          [UIColor colorWithRed: r / 255.0 green: g / 255.0 blue: b / 255.0 alpha: a]
#define RGBColor(r, g, b)              RGBAColor(r, g, b, 1.0)
#define GrayColor(f)                   [UIColor colorWithWhite: f / 255.0 alpha: 1.0]
#define GrayColorWithAlpha(f, a)       [UIColor colorWithWhite: f / 255.0 alpha: a]


@interface UIColor (CoCoBase)
+ (UIColor *)colorWithHex:(UInt32)hex;
+ (UIColor *)colorWithHex:(UInt32)hex andAlpha:(CGFloat)alpha;

+ (UIColor *)colorWithHexString:(NSString *)hexString;


//不要除255
+ (UIColor *)colorWithWholeRed:(CGFloat)red
    green:(CGFloat)green
    blue:(CGFloat)blue
    alpha:(CGFloat)alpha;

//不要除255
+ (UIColor *)colorWithWholeRed:(CGFloat)red
    green:(CGFloat)green
    blue:(CGFloat)blue;

- (NSString *)coco_hexString;
@end
