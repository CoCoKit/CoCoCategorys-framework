//
//  UIScrollView+CoCoAnimation.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/4/14.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIScrollView (CoCoAnimation)
- (void)coco_scrollToTopAnimated:(BOOL)animated;
@end
