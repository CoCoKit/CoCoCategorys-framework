//
//  NSDateFormatter+CoCoBase.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/2.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (CoCoBase)
+ (id)coco_dateFormatter;
+ (id)coco_dateFormatterWithFormat:(NSString *)dateFormat;

+ (id)coco_defaultDateFormatter;/*yyyy-MM-dd HH:mm:ss*/
@end
