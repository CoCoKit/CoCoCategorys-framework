//
//  UIImage+CoCoOrientation.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CoCoOrientation)
- (UIImage *)coco_fixOrientation;
@end
