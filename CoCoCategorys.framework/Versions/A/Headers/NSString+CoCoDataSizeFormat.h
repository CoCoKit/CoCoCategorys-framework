//
//  NSString+CoCoDataSizeFormat.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoDataSizeFormat)
- (NSString *)coco_stringFormatDataSize:(unsigned long long)fileSize;
@end
