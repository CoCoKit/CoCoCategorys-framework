//
//  NSData+CoCoSubData.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/2.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SubDataOption)(NSData *data, NSInteger num, NSRange subRange);

@interface NSData (CoCoSubData)
/** 回传数据：data，第几个：num 以及range */
- (void)coco_subDataWithCount:(NSInteger )count option:(SubDataOption )option;


+ (NSData *)coco_dataOfFile:(NSString *)filePath offset:(unsigned long long)offset length:(unsigned long long)length;

@end
