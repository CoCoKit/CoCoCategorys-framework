//
//  UIScrollView+CoCoBase.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM (NSInteger, CoCoScrollDirection) {
    CoCoScrollDirectionUp,
    CoCoScrollDirectionDown,
    CoCoScrollDirectionLeft,
    CoCoScrollDirectionRight,
    CoCoScrollDirectionWTF
};

@interface UIScrollView (CoCoBase)
@property (nonatomic) CGFloat coco_contentWidth;
@property (nonatomic) CGFloat coco_contentHeight;
@property (nonatomic) CGFloat coco_contentOffsetX;
@property (nonatomic) CGFloat coco_contentOffsetY;

- (CGPoint)coco_topContentOffset;
- (CGPoint)coco_bottomContentOffset;
- (CGPoint)coco_leftContentOffset;
- (CGPoint)coco_rightContentOffset;

- (CoCoScrollDirection)coco_ScrollDirection;

- (BOOL)coco_isScrolledToTop;
- (BOOL)coco_isScrolledToBottom;
- (BOOL)coco_isScrolledToLeft;
- (BOOL)coco_isScrolledToRight;
- (void)coco_scrollToTopAnimated:(BOOL)animated;
- (void)coco_scrollToBottomAnimated:(BOOL)animated;
- (void)coco_scrollToLeftAnimated:(BOOL)animated;
- (void)coco_scrollToRightAnimated:(BOOL)animated;

- (NSUInteger)coco_verticalPageIndex;
- (NSUInteger)coco_horizontalPageIndex;

- (void)coco_scrollToVerticalPageIndex:(NSUInteger)pageIndex animated:(BOOL)animated;
- (void)coco_scrollToHorizontalPageIndex:(NSUInteger)pageIndex animated:(BOOL)animated;
@end
