//
//  UIColor+CoCoInterpolate.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/7/7.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoCoCategorys/CoCoInterpolatable.h>

@interface UIColor (CoCoInterpolate) <CoCoInterpolatable>
- (UIColor *)interpolateTo:(UIColor *)toValue withProgress:(CGFloat)progress;
@end
