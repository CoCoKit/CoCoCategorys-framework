//
//  UIButton+CoCoActivityIndicator.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (CoCoActivityIndicator)
/**
   This method will show the activity indicator in place of the button text.
 */
- (void)coco_showIndicator;

/**
   This method will remove the indicator and put thebutton text back in place.
 */
- (void)coco_hideIndicator;
@end
