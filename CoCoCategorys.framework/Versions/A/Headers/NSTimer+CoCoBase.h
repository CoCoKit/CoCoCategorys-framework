//
//  NSTimer+CoCoBase.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (CoCoBase)
// scheduled的初始化方法将以默认mode直接添加到当前的runloop中.
+ (id)coco_runTimer:(NSTimeInterval)inTimeInterval block:(void (^)(void))inBlock rep:(BOOL)inRepeats;
//不用scheduled方式初始化的，需要手动addTimer:forMode: 将timer添加到一个runloop中。   [[NSRunLoop currentRunLoop]addTimer:he forMode:NSDefaultRunLoopMode];
+ (id)coco_timerWithTimeInterval:(NSTimeInterval)inTimeInterval block:(void (^)(void))inBlock repeats:(BOOL)inRepeats;

/**
 *  暂停
 */
- (void)coco_pause;
/**
 *  继续
 */
- (void)coco_goOn;
/**
 *  X秒后继续
 */
- (void)coco_goOn:(NSTimeInterval)interval;
@end
