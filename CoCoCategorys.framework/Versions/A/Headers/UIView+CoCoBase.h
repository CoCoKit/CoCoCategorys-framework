//
//  UIView+CoCoBase.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>


#ifdef __cplusplus
extern "C" {
#endif
UIColor* coco_theme_color(void);
extern inline BOOL CoCoiPhoneX(void);
extern inline CGFloat CoCoBottomSafeArea(void);
extern inline CGFloat CoCoStatusBarArea(void);
#ifdef __cplusplus
}
#endif



@interface UIView (CoCoBase)


/*----------------------
* Absolute coordinate *
   ----------------------*/
// shortcuts for frame properties
@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGSize size;

// shortcuts for positions
@property (nonatomic) CGFloat centerX;
@property (nonatomic) CGFloat centerY;


@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic) CGFloat top;
@property (nonatomic) CGFloat bottom;
@property (nonatomic) CGFloat right;
@property (nonatomic) CGFloat left;

/**
 *  本组为设置View的坐标, 不会改变View的大小. "toSuperView***"在superView不存在的情况下默认为keyWindow.
 */
@property (nonatomic, assign) CGFloat minX;
@property (nonatomic, assign) CGFloat minY;
@property (nonatomic, assign) CGFloat midX;
@property (nonatomic, assign) CGFloat midY;
@property (nonatomic, assign) CGFloat maxX;
@property (nonatomic, assign) CGFloat maxY;

@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;

@property (nonatomic, assign) CGFloat toSuperViewLeft;
@property (nonatomic, assign) CGFloat toSuperViewRight;
@property (nonatomic, assign) CGFloat toSuperViewTop;
@property (nonatomic, assign) CGFloat toSuperViewBottom;


/*----------------------
* Relative coordinate *
   ----------------------*/

@property (nonatomic, readonly) CGFloat middleX;
@property (nonatomic, readonly) CGFloat middleY;
@property (nonatomic, readonly) CGPoint middlePoint;
@end
