//
//  UIButton+CoCoBackgroundColor.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (CoCoBackgroundColor)
/**
 *  @brief  使用颜色设置按钮背景
 *
 *  @param backgroundColor 背景颜色
 *  @param state           按钮状态
 */
- (void)coco_setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state;
@end
