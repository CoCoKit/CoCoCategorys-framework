//
//  NSString+CoCoSimilarity.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>
#import <CoreGraphics/CGBase.h>

typedef NS_OPTIONS (NSUInteger, NSStringCoCoSimilarityOption) {
    NSStringCoCoSimilarityOptionNone                     = 1 << 0,
    NSStringCoCoSimilarityOptionFavorSmallerWords        = 1 << 1,
    NSStringCoCoSimilarityOptionReducedLongStringPenalty = 1 << 2
};

// 模糊匹配字符串 查找某两个字符串的相似程度
@interface NSString (CoCoSimilarity)
- (CGFloat)coco_scoreAgainst:(NSString *)otherString;
- (CGFloat)coco_scoreAgainst:(NSString *)otherString fuzziness:(NSNumber *)fuzziness;
- (CGFloat)coco_scoreAgainst:(NSString *)otherString fuzziness:(NSNumber *)fuzziness options:(NSStringCoCoSimilarityOption)options;
@end
