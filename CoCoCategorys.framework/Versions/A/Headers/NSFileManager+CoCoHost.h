//
//  NSFileManager+CoCoHost.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/4/26.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (CoCoHost)
/**
 * 一般需要持久的数据都放在此目录中，可以在当中添加子文件夹，iTunes备份和恢复的时候，会包括此目录。
 */
+ (NSString *)documentPath;


/**
 * 设置程序的默认设置和其他状态信息
 */
+ (NSString *)libraryPath;


/**
 * 创建临时文件的目录，当iOS设备重启时，文件会被自动清除
 */
+ (NSString *)tempPath;


/**
 * iTunes不会备份，用来存储应用程序下次启动时需要的数据，一般是缓存文件
 */
+ (NSString *)cachePath;

/**
 * 获取剩余磁盘空间，单位：byte
 */
+ (long)diskFreeSize;
@end
