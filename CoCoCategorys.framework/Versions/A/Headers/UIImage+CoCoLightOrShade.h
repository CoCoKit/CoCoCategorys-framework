//
//  UIImage+CoCoLightOrShade.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CoCoLightOrShade)
// 获取图片亮度，与128对比，如果大于128，就是亮，小于就是暗
- (void)imageBrightness:(void (^)(CGFloat brightness))block withTiles:(int)tilesNum;
@end
