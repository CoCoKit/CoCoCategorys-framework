//
//  NSFileManager+CoCoFile.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/4/26.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (CoCoFile)
/**
 * 创建一个空文件
 */
+ (BOOL)coco_createEmptyFileIfNotExit:(NSString *)path;

/**
 * 创建文件并写入NSData
 */
+ (BOOL)coco_createFileIfNotExit:(NSString *)path data:(NSData *)data;

+ (unsigned long long)coco_fileSizeWithPath:(NSString *)path;

+ (BOOL)coco_existItemAtPath:(NSString *)path;

+ (NSArray *)coco_filesNameInFolder:(NSString *)path;

+ (BOOL)coco_removeItemAtPath:(NSString *)path;

+ (BOOL)coco_removeAllFileAtPath:(NSString *)path;

+ (NSArray *)coco_pathsForItemsMatchingExtension:(NSString *)ext inFolder:(NSString *)path;

+ (BOOL)coco_copyItemAtPath:(NSString *)fromPath toPath:(NSString *)toPath overWrite:(BOOL)overWrite;

+ (BOOL)coco_writeDic:(NSDictionary *)dic toFile:(NSString *)filePath;

+ (NSDictionary *)coco_readDicFromFile:(NSString *)filePath;
@end
