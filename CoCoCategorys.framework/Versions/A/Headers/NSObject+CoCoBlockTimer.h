//
//  NSObject+CoCoBlockTimer.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (CoCoBlockTimer)
#ifdef DEBUG
- (void)coco_logTimeTakenToRunBlock:(void (^)(void))block withPrefix:(NSString *)prefixString;
#endif
@end
