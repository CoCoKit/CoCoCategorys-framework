//
//  UIScreen+CoCoFrame.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CoCo_Screen_Width  [UIScreen CoCo_Width]
#define CoCo_Screen_Height [UIScreen CoCo_Height]
#define CoCo_Screen_Size   [UIScreen CoCo_Size]

@interface UIScreen (CoCoFrame)

+ (CGSize)CoCo_Size;
+ (CGFloat)CoCo_Width;
+ (CGFloat)CoCo_Height;

+ (CGSize)CoCo_OrientationSize;
+ (CGFloat)CoCo_OrientationWidth;
+ (CGFloat)CoCo_OrientationHeight;
+ (CGSize)CoCo_DPISize;


@end
