//
//  NSDate+CoCoTimeToNowString.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/2.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (CoCoTimeToNowString)
- (NSString *)coco_timeToNow;
@end
