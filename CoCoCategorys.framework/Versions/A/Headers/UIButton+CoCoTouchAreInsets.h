//
//  UIButton+CoCoTouchAreInsets.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (CoCoTouchAreInsets)
/**
 *  @brief  设置按钮额外热区
 */
- (void)setJk_touchAreaInsets:(UIEdgeInsets)touchAreaInsets;
@end
