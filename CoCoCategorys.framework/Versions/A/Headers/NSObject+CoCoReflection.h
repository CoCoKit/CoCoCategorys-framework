//
//  NSObject+CoCoReflection.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (CoCoReflection)
// 类名
- (NSString *)coco_className;
+ (NSString *)coco_className;
// 父类名称
- (NSString *)coco_superClassName;
+ (NSString *)coco_superClassName;

// 实例属性字典
- (NSDictionary *)coco_propertyDictionary;

// 属性名称列表
- (NSArray *)coco_propertyKeys;
+ (NSArray *)coco_propertyKeys;

// 属性详细信息列表
- (NSArray *)coco_propertiesInfo;
+ (NSArray *)coco_propertiesInfo;

// 格式化后的属性列表
+ (NSArray *)coco_propertiesWithCodeFormat;

// 方法列表
- (NSArray *)coco_methodList;
+ (NSArray *)coco_methodList;

- (NSArray *)coco_methodListInfo;

// 创建并返回一个指向所有已注册类的指针列表
+ (NSArray *)coco_registedClassList;
// 实例变量
+ (NSArray *)coco_instanceVariable;

// 协议列表
- (NSDictionary *)coco_protocolList;
+ (NSDictionary *)coco_protocolList;


- (BOOL)coco_hasPropertyForKey:(NSString *)key;
- (BOOL)coco_hasIvarForKey:(NSString *)key;
@end
