//
//  NSString+CoCoGBK.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/2.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoGBK)
/**
 *  从GBK编码转换成UTF-8编码的NSString
 *
 *
 *  @return string encoded with UTF-8
 */

- (instancetype)initWithGBKString:(const char *)nullTerminatedCString;
- (instancetype)initWithGBKData:(NSData *)data;

+ (instancetype)CoCo_stringWithGBKString:(const char *)nullTerminatedCString;
+ (instancetype)CoCo_stringWithGBKData:(NSData *)data;
@end
