//
//  CALayer+CoCoTag.h
//  CoCoCategorys
//
//  Created by iScarlett CoCo on 2018/8/9.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface CALayer (CoCoTag)
@property (nonatomic, assign) NSInteger tag;

- (void)setTag:(NSInteger)tag;
- (NSInteger)tag;
@end

NS_ASSUME_NONNULL_END
