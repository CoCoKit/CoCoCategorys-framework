//
//  UIScrollView+CoCoPage.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (CoCoPage)
- (NSInteger)coco_pages;
- (NSInteger)coco_currentPage;
- (CGFloat)coco_scrollPercent;

- (CGFloat)coco_pagesY;
- (CGFloat)coco_pagesX;
- (CGFloat)coco_currentPageY;
- (CGFloat)coco_currentPageX;
- (void)coco_setPageY:(CGFloat)page;
- (void)coco_setPageX:(CGFloat)page;
- (void)coco_setPageY:(CGFloat)page animated:(BOOL)animated;
- (void)coco_setPageX:(CGFloat)page animated:(BOOL)animated;
@end
