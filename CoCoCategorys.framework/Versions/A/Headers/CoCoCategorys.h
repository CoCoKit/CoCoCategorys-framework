//
//  CoCoCategorys.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/2.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoCoCategorys/CoCoCategoryMacros.h>
#import <CoCoCategorys/CoCoMacroConfig.h>
#import <CoCoCategorys/CoCoInterpolatable.h>
/**
 * Foundation
 */

#import <CoCoCategorys/CoCoCategoryMacros.h>
#import <CoCoCategorys/CoCoMacroConfig.h>
#import <CoCoCategorys/NSValue+CoCoInterpolate.h>
#import <CoCoCategorys/NSDictionary+CoCoMerge.h>
#import <CoCoCategorys/NSDictionary+CoCoJSON.h>
#import <CoCoCategorys/NSDictionary+CoCoEnumer.h>
#import <CoCoCategorys/NSDictionary+CoCoURL.h>
#import <CoCoCategorys/NSDictionary+CoCoXML.h>
#import <CoCoCategorys/NSString+CoCoSafely.h>
#import <CoCoCategorys/NSObject+CoCoSafeBlock.h>
#import <CoCoCategorys/NSObject+CoCoCopy.h>
#import <CoCoCategorys/NSObject+CoCoTimer.h>
#import <CoCoCategorys/NSObject+CoCoAccociated.h>
#import <CoCoCategorys/NSArray+CoCoBase.h>
#import <CoCoCategorys/NSArray+CoCoSafe.h>
#import <CoCoCategorys/NSString+CoCoRandom.h>
#import <CoCoCategorys/NSMutableArray+CoCoSafe.h>
#import <CoCoCategorys/NSData+CoCoSubData.h>
#import <CoCoCategorys/NSData+CoCoMD5.h>
#import <CoCoCategorys/NSDate+CoCoBase.h>
#import <CoCoCategorys/NSDateFormatter+CoCoBase.h>
#import <CoCoCategorys/NSDate+CoCoTimeToNowString.h>
#import <CoCoCategorys/NSData+CoCoCache.h>
#import <CoCoCategorys/NSNumber+CoCoInterpolate.h>
#import <CoCoCategorys/NSString+CoCoDate.h>
#import <CoCoCategorys/NSString+CoCoGBK.h>
#import <CoCoCategorys/NSString+CoCoMD5.h>
#import <CoCoCategorys/NSString+CoCoTimestamp.h>
#import <CoCoCategorys/NSString+CoCoIDCard.h>
#import <CoCoCategorys/NSString+CoCoEmail.h>
#import <CoCoCategorys/NSString+CoCoDataSizeFormat.h>
#import <CoCoCategorys/NSString+CoCoSize.h>
#import <CoCoCategorys/NSString+CoCoDirFileSize.h>
#import <CoCoCategorys/NSString+CoCoSHA256.h>
#import <CoCoCategorys/NSString+CoCoURL.h>
#import <CoCoCategorys/NSString+CoCoUUID.h>
#import <CoCoCategorys/NSString+CoCoNumber.h>
#import <CoCoCategorys/NSString+CoCoChar.h>
#import <CoCoCategorys/NSString+CoCoReverse.h>
#import <CoCoCategorys/NSString+CoCoTrim.h>
#import <CoCoCategorys/NSString+CoCoDictionary.h>
#import <CoCoCategorys/NSString+CoCoHash.h>
#import <CoCoCategorys/NSString+CoCoHmac.h>
#import <CoCoCategorys/NSString+CoCoBytes.h>
#import <CoCoCategorys/NSString+CoCoSimilarity.h>
#import <CoCoCategorys/NSString+CoCoContains.h>
#import <CoCoCategorys/NSString+CoCoMobileNumber.h>
#import <CoCoCategorys/NSString+CoCoRegex.h>
#import <CoCoCategorys/NSString+CoCoCarNumber.h>
#import <CoCoCategorys/NSString+CoCoMacAddress.h>
#import <CoCoCategorys/NSString+CoCoChinese.h>
#import <CoCoCategorys/NSString+CoCoPostalCode.h>
#import <CoCoCategorys/NSString+CoCoPassword.h>
#import <CoCoCategorys/NSString+CoCoBankCard.h>
#import <CoCoCategorys/NSString+CoCoIPAddress.h>
#import <CoCoCategorys/NSString+CoCoXML.h>
#import <CoCoCategorys/NSString+CoCoHtml.h>
#import <CoCoCategorys/NSString+CoCoPath.h>
#import <CoCoCategorys/NSAttributedString+CoCoSize.h>
#import <CoCoCategorys/NSFileManager+CoCoFile.h>
#import <CoCoCategorys/NSFileManager+CoCoAsyncFile.h>
#import <CoCoCategorys/NSFileManager+CoCoHost.h>
#import <CoCoCategorys/NSFileManager+CoCoFileMD5.h>
#import <CoCoCategorys/NSFileManager+CoCoPath.h>

/**
 * UIKit
 */

#import <CoCoCategorys/UIApplication+CoCoPermissions.h>
#import <CoCoCategorys/UIApplication+CoCoBase.h>
#import <CoCoCategorys/UIButton+CoCoBadge.h>
#import <CoCoCategorys/UIButton+CoCoImagePosition.h>
#import <CoCoCategorys/UIButton+CoCoMiddleAlign.h>
#import <CoCoCategorys/UIButton+CoCoTouchArea.h>
#import <CoCoCategorys/UIButton+CoCoActivityIndicator.h>
#import <CoCoCategorys/UIButton+CoCoCountDown.h>
#import <CoCoCategorys/UIButton+CoCoBackgroundColor.h>
#import <CoCoCategorys/UIButton+CoCoSubmitting.h>
#import <CoCoCategorys/UIColor+CoCoBase.h>
#import <CoCoCategorys/UIColor+CoCoRandom.h>
#import <CoCoCategorys/UIColor+CoCoGradient.h>
#import <CoCoCategorys/UIColor+CoCoInterpolate.h>
#import <CoCoCategorys/UIControl+CoCoSound.h>
#import <CoCoCategorys/UIControl+CoCoActionBlock.h>
#import <CoCoCategorys/UIDevice+CoCoBase.h>
#import <CoCoCategorys/UIDevice+Version.h>
#import <CoCoCategorys/UIDevice+CoCoPath.h>
#import <CoCoCategorys/UIFont+CoCoLoader.h>
#import <CoCoCategorys/UIImage+CoCoSize.h>
#import <CoCoCategorys/UIImage+CoCoColor.h>
#import <CoCoCategorys/UIImage+CoCoSnapshot.h>
#import <CoCoCategorys/UIImage+CoCoLoader.h>
#import <CoCoCategorys/UIImage+CoCoLightOrShade.h>
#import <CoCoCategorys/UIImage+CoCoTintColor.h>
#import <CoCoCategorys/UIScreen+CoCoFrame.h>
#import <CoCoCategorys/UIScreen+CoCoBase.h>
#import <CoCoCategorys/UIScrollView+CoCoPage.h>
#import <CoCoCategorys/UIScrollView+CoCoBase.h>
#import <CoCoCategorys/UITextField+CoCoShake.h>
#import <CoCoCategorys/UITextField+CoCoSelect.h>
#import <CoCoCategorys/UITextField+CoCoActionBlock.h>
#import <CoCoCategorys/UITextView+CoCoPlaceHolder.h>
#import <CoCoCategorys/UIView+CoCoBase.h>
#import <CoCoCategorys/UIView+CoCoSnapshot.h>
#import <CoCoCategorys/UIView+CoCoNib.h>
#import <CoCoCategorys/UIView+CoCoGesture.h>
#import <CoCoCategorys/UIView+CoCoFind.h>
#import <CoCoCategorys/UIWindow+CoCoViewController.h>
#import <CoCoCategorys/UIView+CoCoViewController.h>

#import <CoCoCategorys/CALayer+CoCoAnimation.h>
#import <CoCoCategorys/CALayer+CoCoTag.h>
#import <CoCoCategorys/UIView+CoCoTouchHighlighting.h>
#import <CoCoCategorys/UIAlertController+CoCoAction.h>
#import <CoCoCategorys/NSString+CoCoVersion.h>
#import <CoCoCategorys/UIImage+CoCoOrientation.h>
#import <CoCoCategorys/UIBarButtonItem+CoCoFixiOS11.h>
#import <CoCoCategorys/NSTimer+CoCoBase.h>
#import <CoCoCategorys/CATransaction+CoCoAnimation.h>
#import <CoCoCategorys/NSObject+CoCoReflection.h>
#import <CoCoCategorys/NSObject+CoCoBlock.h>
#import <CoCoCategorys/NSObject+CoCoRuntime.h>
#import <CoCoCategorys/NSObject+CoCoBlockTimer.h>
#import <CoCoCategorys/UIBezierPath+CoCoLength.h>
#import <CoCoCategorys/UIBezierPath+CoCoThroughPointsBezier.h>
#import <CoCoCategorys/UITextField+CoCoInputLimit.h>
#import <CoCoCategorys/UIImageView+CoCoReflect.h>
#import <CoCoCategorys/UIImageView+CoCoFaceAwareFill.h>
#import <CoCoCategorys/UIImageView+CoCoGeometryConversion.h>
#import <CoCoCategorys/UIImageView+CoCoBetterFace.h>
#import <CoCoCategorys/UIImage+CoCoAlpha.h>
#import <CoCoCategorys/UIImage+CoCoRoundedCorner.h>
#import <CoCoCategorys/UIImage+CoCoBlur.h>
#import <CoCoCategorys/UIButton+CoCoAction.h>
#import <CoCoCategorys/UITableViewCell+CoCoNib.h>
#import <CoCoCategorys/UITableView+CoCoBase.h>
#import <CoCoCategorys/NSRunLoop+CoCoPerformBlock.h>
#import <CoCoCategorys/NSData+CoCoBase64.h>
#import <CoCoCategorys/NSString+CoCoBase64.h>
#import <CoCoCategorys/NSObject+CoCoKVO.h>

// ! Project version number for CoCoCategorys.
FOUNDATION_EXPORT double CoCoCategorysVersionNumber;

// ! Project version string for CoCoCategorys.
FOUNDATION_EXPORT const unsigned char CoCoCategorysVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoCoCategorys/PublicHeader.h>
