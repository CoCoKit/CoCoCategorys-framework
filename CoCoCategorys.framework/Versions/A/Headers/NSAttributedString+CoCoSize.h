//
//  NSAttributedString+CoCoSize.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/2/22.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGBase.h>

@interface NSAttributedString (CoCoSize)
- (CGFloat)heightForWidth:(CGFloat)width;
@end
