//
//  UITextField+CoCoActionBlock.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (CoCoActionBlock)
@property (copy, nonatomic) BOOL (^ coco_shouldBegindEditingBlock)(UITextField *textField);
@property (copy, nonatomic) BOOL (^ coco_shouldEndEditingBlock)(UITextField *textField);
@property (copy, nonatomic) void (^ coco_didBeginEditingBlock)(UITextField *textField);
@property (copy, nonatomic) void (^ coco_didEndEditingBlock)(UITextField *textField);
@property (copy, nonatomic) BOOL (^ coco_shouldChangeCharactersInRangeBlock)(UITextField *textField, NSRange range, NSString *replacementString);
@property (copy, nonatomic) BOOL (^ coco_shouldReturnBlock)(UITextField *textField);
@property (copy, nonatomic) BOOL (^ coco_shouldClearBlock)(UITextField *textField);

- (void)coco_setShouldBegindEditingBlock:(BOOL (^)(UITextField *textField))shouldBegindEditingBlock;
- (void)coco_setShouldEndEditingBlock:(BOOL (^)(UITextField *textField))shouldEndEditingBlock;
- (void)coco_setDidBeginEditingBlock:(void (^)(UITextField *textField))didBeginEditingBlock;
- (void)coco_setDidEndEditingBlock:(void (^)(UITextField *textField))didEndEditingBlock;
- (void)coco_setShouldChangeCharactersInRangeBlock:(BOOL (^)(UITextField *textField,
                                                             NSRange range,
                                                             NSString *string))shouldChangeCharactersInRangeBlock;
- (void)coco_setShouldClearBlock:(BOOL (^)(UITextField *textField))shouldClearBlock;
- (void)coco_setShouldReturnBlock:(BOOL (^)(UITextField *textField))shouldReturnBlock;
@end
