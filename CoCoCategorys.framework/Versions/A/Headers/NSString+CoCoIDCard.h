//
//  NSString+CoCoIDCard.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoIDCard)
- (BOOL)coco_isIDCard;
// 精确的身份证号码有效性检测
- (BOOL)coco_accurateVerifyIDCardNumber;
@end
