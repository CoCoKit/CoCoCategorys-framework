//
//  NSString+CoCoDate.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/2.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoDate)

/**
 * yyyy-MM-dd HH:mm:ss
 * 转换成nsdate
 */
@property (nonatomic, strong, readonly) NSDate *coco_date;

/**
 * 内部先转换为NSDate，然后再格式化成(几分钟前，几小时前，几天前)等
 */
@property (nonatomic, strong, readonly) NSString *coco_displayDate;


/** 只显示日期, 今天, 昨天, 前天, yy.dd */
/**
 * 今天、昨天、前天 、 12.13
 */
@property (nonatomic, strong, readonly) NSString *coco_displayStringIgnoreTime;

/** 只显示时间, N分钟前 hh:mm */
@property (nonatomic, strong, readonly) NSString *coco_displayStringIgnoreDate;
@end
