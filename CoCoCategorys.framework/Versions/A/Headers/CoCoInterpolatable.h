//
//  CoCoInterpolatable.h
//  WeiboCoCo
//
//  Created by 陈明 on 2017/7/9.
//  Copyright © 2017年 com.weibococo. All rights reserved.
//

#ifndef CoCoInterpolatable_h
#define CoCoInterpolatable_h


@protocol CoCoInterpolatable <NSObject>

- (id)interpolateTo:(id)toValue withProgress:(CGFloat)progress;

@end

#endif /* CoCoInterpolatable_h */
