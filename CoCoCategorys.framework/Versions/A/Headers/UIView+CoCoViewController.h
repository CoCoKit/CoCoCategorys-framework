//
//  UIView+CoCoViewController.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/4/14.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CoCoViewController)
/**
 * 获取当前视图的viewController，如果没有controller，则返回null
 */
- (UIViewController *)coco_viewController;
@end
