//
//  UITableView+CoCoBase.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/20.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UITableView (CoCoBase)


/**
 * 初始化一个group类型的tableview
 */
+ (instancetype)coco_groupStyle;
+ (instancetype)coco_groupStyleWithFrame:(CGRect)frame;
/**
 * 初始化一个plain类型的tableview
 */
+ (instancetype)coco_plainStyle;
+ (instancetype)coco_plainStyleWithFrame:(CGRect)frame;

/**
 * 根据类名注册一个nib，identity为Class的名称
 */
- (void)coco_registerFromXibClass:(Class)cls;
// 多个
- (void)coco_registerFromXibClasses:(Class)cls, ... NS_REQUIRES_NIL_TERMINATION;

/**
 * 根据类名注册一个类，identity为Class的名称
 */
- (void)coco_registerClass:(Class)cls;
// 多个
- (void)coco_registerClasses:(Class)cls, ... NS_REQUIRES_NIL_TERMINATION;

/**
 * 注册一个空白Cell
 */
- (void)coco_registerSpaceCell;

/**
 * 根据类获取一个cell
 */
- (UITableViewCell *)coco_cellWithClass:(Class)cls;

- (UITableViewCell *)coco_cellWithIdentifier:(NSString *)identifier;

- (UITableViewCell *)coco_spaceCell;

- (void)coco_updateWithBlock:(void (^)(UITableView *tableView))block;

@end
