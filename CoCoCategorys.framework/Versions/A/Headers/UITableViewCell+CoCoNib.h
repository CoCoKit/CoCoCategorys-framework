//
//  UITableViewCell+CoCoNib.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/20.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (CoCoNib)
+ (UINib *)coco_nib;
@end
