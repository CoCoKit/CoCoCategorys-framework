//
//  NSString+CoCoCarNumber.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoCarNumber)
// 车牌
- (BOOL)coco_isCarNumber;
@end
