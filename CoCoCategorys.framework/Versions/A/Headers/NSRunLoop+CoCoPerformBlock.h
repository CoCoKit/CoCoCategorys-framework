//
//  NSRunLoop+CoCoPerformBlock.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/20.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const CoCoNSRunloopTimeoutException;

@interface NSRunLoop (CoCoPerformBlock)
- (void)coco_performBlockAndWait:(void (^)(BOOL *finish))block;
- (void)coco_performBlockAndWait:(void (^)(BOOL *finish))block timeoutInterval:(NSTimeInterval)timeoutInterval;
@end
