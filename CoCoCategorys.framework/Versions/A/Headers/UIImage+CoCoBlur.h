//
//  UIImage+CoCoBlur.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CoCoBlur)
- (UIImage *)coco_lightImage;
- (UIImage *)coco_extraLightImage;
- (UIImage *)coco_darkImage;
- (UIImage *)coco_tintedImageWithColor:(UIColor *)tintColor;

- (UIImage *)coco_blurredImageWithRadius:(CGFloat)blurRadius;
- (UIImage *)coco_blurredImageWithSize:(CGSize)blurSize;
- (UIImage *)coco_blurredImageWithSize:(CGSize)blurSize
    tintColor:(UIColor *)tintColor
    saturationDeltaFactor:(CGFloat)saturationDeltaFactor
    maskImage:(UIImage *)maskImage;
@end
