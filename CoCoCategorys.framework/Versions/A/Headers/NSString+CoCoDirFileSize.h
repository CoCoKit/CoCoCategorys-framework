//
//  NSString+CoCoDirFileSize.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^CoCoDirFileSizeBlock)(long size);
typedef void (^CoCoDirClearCacheBlock)(void);

@interface NSString (CoCoDirFileSize)

- (long long)coco_filesSize;
@end
