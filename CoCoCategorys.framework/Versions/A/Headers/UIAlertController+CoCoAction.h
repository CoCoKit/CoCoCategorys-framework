//
//  UIAlertController+CoCoAction.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/17.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^CoCoAlertTextFiledHanler)(NSString *_Nullable text);

@interface UIAlertController (CoCoAction)
/**
   to add UIAlertAction with UIAlertActionStyleDefault

   @param title - the title of UIAlertAction
   @param handler - to handle your business
 */
- (void)coco_addAlertDefaultActionWithTitle:(NSString *_Nullable)title
    handler:(void (^_Nullable)(UIAlertAction *_Nullable action))handler;


/**
   to add UIAlertAction with Custom Style


   @param title - the title of UIAlertAction
   @param actionStyle - to chose UIAlertActionStyle
   @param handler - to handle your business
 */
- (void)coco_addAlertActionWithTitle:(NSString *_Nullable)title
    actionStyle:(UIAlertActionStyle)actionStyle
    handler:(void (^__nullable)(UIAlertAction *_Nullable action))handler;


/**
   to add TextField in your alert , callback the  text which  you input
   it only support in Alert Styple

   @param placeholder - set TextField's placeholder
   @param secureTextEntry - set Secure input Mode
   @param textHandler - to get text which  you input
 */
- (void)coco_addTextFieldWithPlaceholder:(NSString *_Nullable)placeholder
    secureTextEntry:(BOOL)secureTextEntry
    textHandler:(CoCoAlertTextFiledHanler _Nullable )textHandler;


/**
   to add TextField in your alert, callback the  textFiled which  you built
   it only support in Alert Styple

   @param placeholder - set TextField's placeholder
   @param secureTextEntry - set Secure input Mode
   @param textFiledhandler - to handle textField which you can do anything
 */
- (void)coco_addTextFieldWithPlaceholder:(NSString *_Nullable)placeholder
    secureTextEntry:(BOOL)secureTextEntry
    textFiledhandler:(void (^_Nullable)(UITextField *_Nonnull textField))textFiledhandler;
@end
