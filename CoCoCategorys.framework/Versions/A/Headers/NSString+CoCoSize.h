//
//  NSString+CoCoSize.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGBase.h>
#import <UIKit/UIFont.h>
#import <UIKit/NSParagraphStyle.h>

@interface NSString (CoCoSize)
- (CGFloat)coco_heightWithFont:(UIFont *)font width:(CGFloat)width;
- (CGFloat)coco_widthWithFont:(UIFont *)font height:(CGFloat)height;
- (CGSize)coco_sizeForFont:(UIFont *)font size:(CGSize)size mode:(NSLineBreakMode)lineBeakMode;
@end
