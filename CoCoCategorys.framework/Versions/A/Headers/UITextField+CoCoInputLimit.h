//
//  UITextField+CoCoInputLimit.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (CoCoInputLimit)
@property (assign, nonatomic)  NSInteger coco_maxLength;// if <=0, no limit
@end
