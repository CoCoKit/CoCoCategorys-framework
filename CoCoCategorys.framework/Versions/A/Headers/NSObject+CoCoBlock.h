//
//  NSObject+CoCoBlock.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (CoCoBlock)
+ (id)coco_performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;
+ (id)coco_performBlock:(void (^)(id arg))block withObject:(id)anObject afterDelay:(NSTimeInterval)delay;
- (id)coco_performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;
- (id)coco_performBlock:(void (^)(id arg))block withObject:(id)anObject afterDelay:(NSTimeInterval)delay;
+ (void)coco_cancelBlock:(id)block;
@end
