//
//  UIColor+CoCoRandom.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CoCoRandom)
+ (UIColor *)CoCo_RandomColor;
@end
