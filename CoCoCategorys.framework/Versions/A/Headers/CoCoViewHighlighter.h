//
//  CoCoViewHighlighter.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIView+CoCoTouchHighlighting.h"

@interface CoCoViewHighlighter : NSObject

- (instancetype)initWithView:(__weak UIView *)view;

@property (nonatomic, assign) CoCoHighlightingStyle highlightingStyle;
@property (nonatomic, assign, getter = isHighlighted) BOOL highlighted;

@end
