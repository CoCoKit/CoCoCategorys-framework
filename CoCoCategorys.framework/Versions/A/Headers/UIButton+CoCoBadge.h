//
//  UIButton+CoCoBadge.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (CoCoBadge)
@property (strong, nonatomic) UILabel *coco_badge;

// Badge value to be display
@property (nonatomic) NSString *coco_badgeValue;
// Badge background color
@property (nonatomic) UIColor *coco_badgeBGColor;
// Badge text color
@property (nonatomic) UIColor *coco_badgeTextColor;
// Badge font
@property (nonatomic) UIFont *coco_badgeFont;
// Padding value for the badge
@property (nonatomic) CGFloat coco_badgePadding;
// Minimum size badge to small
@property (nonatomic) CGFloat coco_badgeMinSize;
// Values for offseting the badge over the BarButtonItem you picked
@property (nonatomic) CGFloat coco_badgeOriginX;
@property (nonatomic) CGFloat coco_badgeOriginY;
// In case of numbers, remove the badge when reaching zero
@property BOOL coco_shouldHideBadgeAtZero;
// Badge has a bounce animation when value changes
@property BOOL coco_shouldAnimateBadge;
@end
