//
//  CoCoOpenUDID.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kCoCoOpenUDIDErrorNone        0
#define kCoCoOpenUDIDErrorOptedOut    1
#define kCoCoOpenUDIDErrorCompromised 2

@interface CoCoOpenUDID : NSObject {
}
+ (NSString *)value;
+ (NSString *)valueWithError:(NSError **)error;
+ (void)setOptOut:(BOOL)optOutValue;

@end
