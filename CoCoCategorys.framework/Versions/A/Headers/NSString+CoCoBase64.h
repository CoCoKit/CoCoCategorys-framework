//
//  NSString+CoCoBase64.h
//  CoCoCategorys
//
//  Created by 陈明 on 2018/5/25.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoBase64)
- (NSString *)coco_base64EncodedString;

+ (NSString *)coco_stringWithBase64EncodedString:(NSString *)base64EncodedString;
@end
