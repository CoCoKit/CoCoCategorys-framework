//
//  NSString+CoCoRandom.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/6/26.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CoCoRandom)
+ (NSString *)randomString:(NSUInteger)length;
@end
