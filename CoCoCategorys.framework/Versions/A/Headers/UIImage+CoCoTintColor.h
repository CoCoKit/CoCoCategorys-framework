//
//  UIImage+CoCoTintColor.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/6/4.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CoCoTintColor)
- (UIImage *)tintImageWithColor:(UIColor *)tintColor;

@end
