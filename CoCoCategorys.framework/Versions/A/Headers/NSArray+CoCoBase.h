//
//  NSArray+CoCoBase.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/2.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSArray (CoCoBase)
/** 数组里不能是模型 */
- (NSString *)coco_toJsonString;
@end


@interface NSMutableArray (CoCoBase)


- (void)coco_removeFirstObject;
- (void)coco_removeLastObject;

// 在第一个位置插入
- (void)coco_pushFirstObject:(id)anObject;

// 移除第一个并返回
- (nullable id)coco_popFirstObject;

// 移除最后一个并返回
- (nullable id)coco_popLastObject;

// 插入一个数组
- (void)coco_insertObjects:(NSArray *)objects atIndex:(NSUInteger)index;

// 翻转
- (void)coco_reverse;

// 洗牌，随机打乱
- (void)coco_shuffle;

/**
 *  数组计算交集
 */
- (NSArray *)coco_arrayForIntersectionWithOtherArray:(NSArray *)otherArray;

/**
 *  数组计算差集(当前减去other中相同的)
 */
- (NSArray *)coco_arrayForMinusWithOtherArray:(NSArray *)otherArray;

/**
 *  接收返回的数组 按 字段 给数组排序
 */
- (NSArray *)coco_sortbyKey:(NSString *)key asc:(BOOL)ascend;
@end
