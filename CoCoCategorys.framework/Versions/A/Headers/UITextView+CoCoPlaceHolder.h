//
//  UITextView+CoCoPlaceHolder.h
//  CoCoCategorys
//
//  Created by 陈明 on 2017/3/3.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (CoCoPlaceHolder)
@property (nonatomic, strong) UITextView *coco_placeHolderTextView;
- (void)coco_addPlaceHolder:(NSString *)placeHolder;
@end
